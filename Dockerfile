FROM node:argon
#Install dependencies
RUN apt-get -y update && apt-get install -y libgmp-dev

#Install stack
RUN wget -qO- https://get.haskellstack.org/ | sh &&\
    stack update

RUN stack setup --resolver lts-6.9

# Create build directory
VOLUME /app_src
WORKDIR /app_src

CMD cd backend &&\
    stack --allow-different-user build &&\
    cd ../frontend &&\
    npm install &&\
    npm run build
