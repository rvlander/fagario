Fagario is a agar.io clone written with functional languages. It aims at exploring if such languages can be use to build browser based mutli-player online game.

* Frontend is written in Elm
* Backend is written in Haskell.


# Installation

```bash
cd ../frontend
npm install
```

# Run dev

## Backend

```
cd backend
stack build
stack exec fagario-backend-exe
```

## Frontend

```bash
npm run watch
```
Open browser to http://localhost:1234/.
