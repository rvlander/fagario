'use strict';

// Require index.html so it gets copied to dist
require('./index.html');

const Elm = require('./Main.elm');

// The third value on embed are the initial values for incomming ports into Elm
const app = Elm.Main.fullscreen({
  hostname: document.location.hostname,
  port_: parseInt(document.location.port)
});
