module Clock
    exposing
        ( Clock
        , withPeriod
        , setPeriod
        , update
        , getTime
        , syncClock
        )

{-| Clock is designed to work with [elm-lang/animation-frame](package.elm-lang.org/packages/elm-lang/animation-frame/latest/AnimationFrame). Your model will get consistently-paced updates, despite fluctuations in frame diffs.
@docs Clock, withPeriod, setPeriod, update
-}

import Time exposing (Time)


{-| A clock.
-}
type Clock
    = Clock
        { lag : Time
        , period : Time
        , time : Time
        }


{-| Create a clock that updates with the given real-time period.
    withDelta (100 * Time.millisecond) -- calls the tick function ten times per second
-}
withPeriod : Time -> Clock
withPeriod period =
    Clock
        { lag = 0
        , period = period
        , time = 0
        }


getTime : Clock -> Time
getTime (Clock { lag, period, time }) =
    time + lag


{-| Change a clock's period to the given value.
-}
setPeriod : Time -> Clock -> Clock
setPeriod period (Clock clock) =
    Clock { clock | period = period }


syncClock : Time -> Clock -> Clock
syncClock time (Clock clock) =
    let
        offset =
            clock.time - time
    in
        Clock
            { clock
                | time = clock.time - offset
                , lag = clock.lag + offset
            }


{-| Called like so:
    update up diff clock model
The diff is a real-time diff, such as what is given by AnimationFrame.diffs. This function will pass the diff to the clock. If the diff causes the clock's counter to increment, then `up` will be called with the counter and the model.
-}
update : (Float -> a -> a) -> Time -> Clock -> a -> ( Clock, a )
update up dt (Clock clock) model =
    let
        reduceLag ( Clock c, m ) =
            if c.lag < c.period then
                ( Clock c, m )
            else
                reduceLag
                    ( Clock
                        { c
                            | lag = c.lag - c.period
                            , time = c.time + c.period
                        }
                    , up (Time.inMilliseconds c.period) m
                    )
    in
        reduceLag
            ( Clock { clock | lag = clock.lag + dt }
            , model
            )
