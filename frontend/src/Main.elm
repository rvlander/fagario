module Main exposing (..)

import Mouse
import WebGL as GL
import Math.Vector2 as Vec2 exposing (..)
import Html
import Html exposing (Html)
import Html.Attributes exposing (width, height)
import Window as Window
import AnimationFrame
import Time
import Task
import Clock
import ServerCommunication exposing (..)
import World
    exposing
        ( World
        , Box
        , updateWorldFull
        , centerFromWorld
        , cullCells
        , cullFood
        )
import View exposing (..)


main : Program ServerConf Model Msg
main =
    Html.programWithFlags
        { init =
            \flags ->
                let
                    model =
                        newModel flags
                in
                    ( model
                    , Cmd.batch
                        [ buildHandcheckCommand model.serverConf
                        , Task.perform ResizeWindow Window.size
                        ]
                    )
        , view = view
        , subscriptions = subscriptions
        , update = update
        }


type Msg
    = MovePlayer Mouse.Position
    | ResizeWindow Window.Size
    | NewMessage String
    | Loop Time.Time


type alias Model =
    { serverConf : ServerConf
    , windowSize : Window.Size
    , world : World
    , viewportBox : Box
    , me : Maybe Int
    , gridMesh : Maybe (GL.Drawable Vertex)
    , clock : Clock.Clock
    , mouse : Mouse.Position
    }


newModel : ServerConf -> Model
newModel serverConf =
    { serverConf = serverConf
    , windowSize =
        { width = 1024
        , height = 768
        }
    , world =
        { cells = []
        , food = []
        , size = vec2 0 0
        }
    , viewportBox =
        { width = 10
        , height = 10
        }
    , me = Nothing
    , gridMesh = Nothing
    , clock = Clock.withPeriod (20 * Time.millisecond)
    , mouse =
        { x = 0
        , y = 0
        }
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update action model =
    case action of
        Loop delta ->
            let
                ( clock, world ) =
                    Clock.update updateWorldFull
                        delta
                        model.clock
                        model.world

                newModel =
                    { model | world = world, clock = clock }
            in
                ( newModel
                , sendMousePosition newModel
                )

        MovePlayer pos ->
            ( { model | mouse = pos }, Cmd.none )

        ResizeWindow dims ->
            ( { model
                | windowSize = dims
                , viewportBox = computeViewportBox dims
              }
            , Cmd.none
            )

        NewMessage msg ->
            case decodeResponse msg of
                Err err ->
                    let
                        nerr =
                            Debug.log err
                    in
                        ( model, Cmd.none )

                Ok (NewWorldSate timest world) ->
                    let
                        syncClock =
                            Clock.syncClock timest model.clock

                        ( newClock, newWorld ) =
                            Clock.update updateWorldFull
                                0
                                syncClock
                                world

                        newModel =
                            { model
                                | world = newWorld
                                , clock = newClock
                                , gridMesh =
                                    case model.gridMesh of
                                        Nothing ->
                                            Just
                                                (gridMesh (vec2 1.0 1.0)
                                                    (vec2 0 0)
                                                    world.size
                                                )

                                        _ ->
                                            model.gridMesh
                            }
                    in
                        ( newModel
                        , Cmd.none
                        )

                Ok (SessionCreatedOK playerId) ->
                    let
                        newModel =
                            { model | me = Just playerId }
                    in
                        ( newModel
                        , sendMousePosition newModel
                        )

                Ok SessionCreatedFailed ->
                    ( model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Mouse.moves MovePlayer
        , Window.resizes ResizeWindow
        , setMessageHandle model.serverConf NewMessage
        , AnimationFrame.diffs Loop
        ]


sendMousePosition : Model -> Cmd a
sendMousePosition model =
    let
        pos =
            model.mouse

        windowSize =
            model.windowSize

        position =
            screenToModel pos
                windowSize
                model.viewportBox
                (centerFromWorld model.me model.world)
    in
        case model.me of
            Just _ ->
                buildPlayerCommand model.serverConf
                    (Clock.getTime model.clock)
                    position

            _ ->
                Cmd.none


view : Model -> Html Msg
view { me, world, windowSize, viewportBox, gridMesh } =
    let
        center =
            centerFromWorld me world

        projectionMatrix =
            ortho2D viewportBox center

        drawGrid =
            case gridMesh of
                Just mg ->
                    [ viewGrid projectionMatrix mg ]

                Nothing ->
                    []

        drawCells =
            world.cells
                |> cullCells center viewportBox
                |> List.map (viewCell projectionMatrix)

        drawFood =
            world.food
                |> cullFood center viewportBox
                |> List.map (viewFoodElement projectionMatrix)
    in
        GL.toHtmlWith
            [ GL.Disable GL.DepthTest
            , GL.Enable GL.Blend
            , GL.BlendFunc ( GL.SrcAlpha, GL.OneMinusSrcAlpha )
            ]
            [ width windowSize.width, height windowSize.height ]
            (List.append drawCells (List.append drawFood drawGrid))
