module View
    exposing
        ( viewCell
        , Vertex
        , screenToModel
        , computeViewportBox
        , gridMesh
        , ortho2D
        , viewGrid
        , viewFoodElement
        )

import Math.Vector2 as Vec2 exposing (..)
import Math.Vector3 as Vec3 exposing (..)
import Math.Matrix4 as Mat4
import WebGL as GL
import World exposing (..)
import Window
import Mouse


type alias Vertex =
    { position : Vec2 }


type alias TexturedVertex =
    { position : Vec2, coord : Vec2 }


mesh : GL.Drawable TexturedVertex
mesh =
    GL.Triangle <|
        [ ( TexturedVertex (vec2 -1 -1) (vec2 -1 -1)
          , TexturedVertex (vec2 1 1) (vec2 1 1)
          , TexturedVertex (vec2 1 -1) (vec2 1 -1)
          )
        , ( TexturedVertex (vec2 -1 -1) (vec2 -1 -1)
          , TexturedVertex (vec2 -1 1) (vec2 -1 1)
          , TexturedVertex (vec2 1 1) (vec2 1 1)
          )
        ]


viewPortSize : Vec2
viewPortSize =
    vec2 35 35


computeViewportBox : Window.Size -> Box
computeViewportBox windowSize =
    let
        viewPortWidth =
            Vec2.getX viewPortSize

        viewPortHeight =
            Vec2.getY viewPortSize

        scaleW =
            toFloat windowSize.width / viewPortWidth

        scaleH =
            toFloat windowSize.height / viewPortHeight

        scale =
            max scaleH scaleW

        actualViewportWidth =
            scaleW / scale / 2 * viewPortWidth

        actualViewportHeight =
            scaleH / scale / 2 * viewPortHeight
    in
        { width = actualViewportWidth
        , height = actualViewportHeight
        }


ortho2D : Box -> Vec2 -> Mat4.Mat4
ortho2D viewportBox center =
    let
        meX =
            Vec2.getX center

        meY =
            Vec2.getY center
    in
        Mat4.makeOrtho2D (meX - viewportBox.width)
            (meX + viewportBox.width)
            (meY - viewportBox.height)
            (meY + viewportBox.height)


position : Float -> Float -> Mat4.Mat4
position x y =
    Mat4.makeTranslate3 x y 0


screenToModel : Mouse.Position -> Window.Size -> Box -> Vec2 -> Vec2
screenToModel mouse windowSize viewportBox center =
    let
        meX =
            Vec2.getX center

        meY =
            Vec2.getY center
    in
        vec2
            (toFloat mouse.x
                * viewportBox.width
                / (toFloat windowSize.width)
                + meX
                - viewportBox.width
                / 2
            )
            (toFloat (windowSize.height - mouse.y)
                * viewportBox.height
                / (toFloat windowSize.height)
                + meY
                - viewportBox.height
                / 2
            )


viewCell : Mat4.Mat4 -> Cell -> GL.Renderable
viewCell projectionMatrix cell =
    let
        me =
            cell.position

        mat =
            Mat4.mul projectionMatrix
                (Mat4.mul (position (Vec2.getX me) (Vec2.getY me))
                    (Mat4.makeScale (vec3 cell.radius cell.radius 1))
                )
    in
        GL.render vertexShaderCircle
            fragmentShaderCircle
            mesh
            { mat = mat
            , color = vec3 1 0 0
            }


viewFoodElement : Mat4.Mat4 -> FoodElement -> GL.Renderable
viewFoodElement projectionMatrix cell =
    let
        me =
            cell.fposition

        mat =
            Mat4.mul projectionMatrix
                (Mat4.mul (position (Vec2.getX me) (Vec2.getY me))
                    (Mat4.makeScale (vec3 0.3 0.3 1))
                )
    in
        GL.render vertexShaderCircle
            fragmentShaderCircle
            mesh
            { mat = mat
            , color = vec3 0 1 0
            }


gridMesh : Vec2 -> Vec2 -> Vec2 -> GL.Drawable Vertex
gridMesh resolution orig target =
    let
        minX =
            min (Vec2.getX target) (Vec2.getX orig)

        minY =
            min (Vec2.getY target) (Vec2.getY orig)

        maxX =
            max (Vec2.getX target) (Vec2.getX orig)

        maxY =
            max (Vec2.getY target) (Vec2.getY orig)

        resX =
            Vec2.getX resolution

        resY =
            Vec2.getY resolution

        nbOfColumns =
            round <| (maxX - minX) / resX

        nbOfLines =
            round <| (maxY - minY) / resY

        verticalLines =
            List.map
                (\x ->
                    let
                        x_ =
                            (toFloat x) * resX
                    in
                        ( Vertex (vec2 x_ minY)
                        , Vertex (vec2 x_ maxY)
                        )
                )
                (List.range 0 nbOfColumns)

        horizontalLines =
            List.map
                (\y ->
                    let
                        y_ =
                            (toFloat y) * resY
                    in
                        ( Vertex (vec2 minX y_)
                        , Vertex (vec2 maxX y_)
                        )
                )
                (List.range 0 nbOfLines)

        gridPoints =
            List.concat [ verticalLines, horizontalLines ]
    in
        GL.Lines gridPoints


viewGrid : Mat4.Mat4 -> GL.Drawable Vertex -> GL.Renderable
viewGrid projectionMatrix grid =
    GL.render vertexShader
        fragmentShader
        grid
        { mat = projectionMatrix
        , color = vec3 0.5 0.5 0.5
        }



-- Shaders


vertexShader : GL.Shader { attr | position : Vec2 } { unif | mat : Mat4.Mat4 } {}
vertexShader =
    [glsl|
attribute vec2 position;
uniform mat4 mat;
void main () {
    gl_Position = mat * vec4(position, 0.0, 1.0);
}
|]


fragmentShader : GL.Shader {} { unif | color : Vec3 } {}
fragmentShader =
    [glsl|
precision mediump float;
uniform vec3 color;
void main () {
    gl_FragColor = vec4(color, 1.0);
}
|]


vertexShaderCircle : GL.Shader { attr | position : Vec2, coord : Vec2 } { unif | mat : Mat4.Mat4 } { vcoord : Vec2.Vec2 }
vertexShaderCircle =
    [glsl|
attribute vec2 position;
attribute vec2 coord;
uniform mat4 mat;
varying vec2 vcoord;
void main () {
    gl_Position = mat *  vec4(position, 0.0, 1.0);
    vcoord = coord.xy;
}
|]


fragmentShaderCircle : GL.Shader {} { unif | color : Vec3 } { vcoord : Vec2.Vec2 }
fragmentShaderCircle =
    [glsl|
precision mediump float;
uniform vec3 color;
varying vec2 vcoord;
void main () {
    bool inCircle = length(vcoord.xy) <= 1.0;
    gl_FragColor = vec4(color, inCircle ? 1.0 : 0.0);
}
|]
