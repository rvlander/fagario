module ServerCommunication
    exposing
        ( ServerConf
        , buildHandcheckCommand
        , buildPlayerCommand
        , decodeResponse
        , Response(..)
        , setMessageHandle
        )

import Math.Vector2 as Vec2 exposing (..)
import Math.Vector3 as Vec3 exposing (..)
import Json.Decode as Decode exposing (field)
import Json.Encode as Encode
import World exposing (..)
import WebSocket
import Time exposing (Time)


type alias ServerConf =
    { hostname : String
    , port_ : Int
    }


type GameCommand
    = SetTarget Time Vec2


type alias Pseudo =
    String


type Command
    = GameCommand GameCommand
    | NewSession Pseudo
    | ResumeSession Int


type Response
    = SessionCreatedOK Int
    | SessionCreatedFailed
    | NewWorldSate Time World


toURI : ServerConf -> String
toURI serverConf =
    "ws://" ++ serverConf.hostname ++ ":" ++ (toString serverConf.port_)


vec2Decoder : Decode.Decoder Vec2
vec2Decoder =
    Decode.map2 vec2
        (field "x" Decode.float)
        (field "y" Decode.float)


colorDecoder : Decode.Decoder Vec3
colorDecoder =
    Decode.map3 vec3
        (field "r" Decode.float)
        (field "g" Decode.float)
        (field "b" Decode.float)


cellDecoder : Decode.Decoder Cell
cellDecoder =
    Decode.map7 Cell
        (field "name" Decode.string)
        (field "position" vec2Decoder)
        (field "velocity" vec2Decoder)
        (field "mass" Decode.float)
        (field "radius" Decode.float)
        (field "color" colorDecoder)
        (field "playerId" Decode.int)


foodElementDecoder : Decode.Decoder FoodElement
foodElementDecoder =
    Decode.map FoodElement
        (field "fposition" vec2Decoder)



--  ("fcolor" := colorDecoder)


worldDecoder : Decode.Decoder World
worldDecoder =
    Decode.map3 World
        (field "cells" <| Decode.list cellDecoder)
        (field "food" <| Decode.list foodElementDecoder)
        (field "size" vec2Decoder)


responseDecoder : Decode.Decoder Response
responseDecoder =
    (field "type" Decode.string) |> Decode.andThen responseType


responseType : String -> Decode.Decoder Response
responseType tag =
    case tag of
        "session_created_ok" ->
            Decode.map SessionCreatedOK (field "playerId" Decode.int)

        "session_created_failed" ->
            Decode.succeed SessionCreatedFailed

        "new_world_state" ->
            Decode.map2 NewWorldSate
                (field "timestamp" Decode.float)
                (field "world" worldDecoder)

        _ ->
            Decode.fail (tag ++ " is not a recognized type for response")



-- cellDecoder : Decoder.Decode Cell


commandEncoder : Command -> Encode.Value
commandEncoder command =
    let
        list =
            case command of
                GameCommand gameCommand ->
                    [ ( "type", Encode.string "game_command" )
                    , ( "game_command", gameCommandEncoder gameCommand )
                    ]

                NewSession pseudo ->
                    [ ( "type", Encode.string "new_session" )
                    , ( "pseudo", Encode.string pseudo )
                    ]

                ResumeSession secret ->
                    [ ( "type", Encode.string "resume_session" )
                    , ( "playerId", Encode.int secret )
                    ]
    in
        list
            |> Encode.object


gameCommandEncoder : GameCommand -> Encode.Value
gameCommandEncoder gameCommand =
    let
        list =
            case gameCommand of
                SetTarget timestamp target ->
                    [ ( "type", Encode.string "set_target" )
                    , ( "timestamp", Encode.float timestamp )
                    , ( "target", positionEncoder target )
                    ]
    in
        list
            |> Encode.object


positionEncoder : Vec2 -> Encode.Value
positionEncoder pos =
    let
        list =
            [ ( "x", Encode.float (Vec2.getX pos) )
            , ( "y", Encode.float (Vec2.getY pos) )
            ]
    in
        list
            |> Encode.object


buildHandcheckCommand : ServerConf -> Cmd a
buildHandcheckCommand serverConf =
    WebSocket.send (toURI serverConf)
        (commandEncoder (NewSession "rvlander")
            |> Encode.encode 0
        )


buildPlayerCommand : ServerConf -> Time -> Vec2 -> Cmd a
buildPlayerCommand serverConf counter position =
    WebSocket.send (toURI serverConf)
        (commandEncoder (GameCommand (SetTarget counter position))
            |> Encode.encode 0
        )


decodeResponse : String -> Result String Response
decodeResponse =
    Decode.decodeString responseDecoder


setMessageHandle : ServerConf -> (String -> a) -> Sub a
setMessageHandle serverConf =
    WebSocket.listen (toURI serverConf)
