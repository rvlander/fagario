module World
    exposing
        ( World
        , cullCells
        , cullFood
        , Box
        , Cell
        , FoodElement
        , updateWorldFull
        , centerFromWorld
        )

import Math.Vector2 as Vec2 exposing (..)
import Math.Vector3 as Vec3 exposing (..)


type alias Cell =
    { name : String
    , position : Vec2
    , velocity : Vec2
    , mass : Float
    , radius : Float
    , color : Vec3
    , playerId : Int
    }


type alias FoodElement =
    { fposition :
        Vec2
        --  , fcolor : Vec3
    }


type alias World =
    { cells : List Cell
    , food : List FoodElement
    , size : Vec2
    }


type alias Box =
    { width : Float
    , height : Float
    }


updateWorldFull : Float -> World -> World
updateWorldFull delta world =
    let
        updateCell cell =
            { cell
                | position =
                    Vec2.add cell.position
                        (Vec2.scale (delta / 20) cell.velocity)
            }
    in
        { world | cells = List.map updateCell world.cells }


cullCells : Vec2 -> Box -> List Cell -> List Cell
cullCells center box =
    List.filter
        (\cell ->
            let
                centerX =
                    Vec2.getX center

                centerY =
                    Vec2.getY center

                minX =
                    centerX - box.width

                maxX =
                    centerX + box.width

                minY =
                    centerY - box.height

                maxY =
                    centerY + box.height

                cellX =
                    Vec2.getX cell.position

                cellY =
                    Vec2.getY cell.position

                radius =
                    cell.radius
            in
                cellX
                    + radius
                    > minX
                    && cellX
                    - radius
                    < maxX
                    && cellY
                    + radius
                    > minY
                    && cellY
                    - radius
                    < maxY
        )


cullFood : Vec2 -> Box -> List FoodElement -> List FoodElement
cullFood center box =
    List.filter
        (\foodElem ->
            let
                centerX =
                    Vec2.getX center

                centerY =
                    Vec2.getY center

                minX =
                    centerX - box.width

                maxX =
                    centerX + box.width

                minY =
                    centerY - box.height

                maxY =
                    centerY + box.height

                cellX =
                    Vec2.getX foodElem.fposition

                cellY =
                    Vec2.getY foodElem.fposition

                radius =
                    0.3
            in
                cellX
                    + radius
                    > minX
                    && cellX
                    - radius
                    < maxX
                    && cellY
                    + radius
                    > minY
                    && cellY
                    - radius
                    < maxY
        )


centerFromWorld : Maybe Int -> World -> Vec2
centerFromWorld mPlayerId world =
    -- TODO search monad stuff for Maybe
    let
        default =
            vec2 (Vec2.getX world.size / 2) (Vec2.getY world.size / 2)
    in
        case mPlayerId of
            Just playerId ->
                case
                    List.head
                        (List.filter (\cell -> playerId == cell.playerId)
                            world.cells
                        )
                of
                    Just a ->
                        a.position

                    _ ->
                        default

            _ ->
                default
