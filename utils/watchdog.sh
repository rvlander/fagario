#!/bin/bash

REGISTRY=registry.gitlab.com/rvlander
REPO=fagario
IMAGE=$REGISTRY/$REPO:latest
PORT=1000

pullres=$(docker pull $IMAGE)

if echo $pullres | grep "up to date"; then
  echo "Latest version already running, pass."
else
  echo "New version available: restarting."
  docker stop $REPO
  docker rm $REPO
  docker run -d --name $REPO -e PORT=$PORT -p $PORT:$PORT --restart=on-failure:10 $IMAGE
fi
