module Utilities (
  mapWithMask,
  clamp
) where

mapWithMask :: (a -> Bool) -> (a -> a) -> [a] -> [a]
mapWithMask _ _ [] = []
mapWithMask p f (a:as) = (if p a then  f a else a) : mapWithMask p f as

clamp :: (Ord a) => a -> a -> a -> a
clamp mn mx = max mn . min mx
