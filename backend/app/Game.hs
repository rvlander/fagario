{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Game (
  newGame,
  Game(..),
  registerUser,
  pushCommand,
  run,
  getCulledWorld,
  getAckTime,
  TimestampedWorld(..)
) where

import           Control.Concurrent
import           Control.Concurrent.STM
import           Control.Monad
import qualified Data.Map               as Map
import           Data.Text              (Text)
import           Debug.Trace
import           System.Clock
import           System.Random          (RandomGen, getStdGen)
import qualified World                  as W

data TimestampedWorld = TimestampedWorld Integer W.World deriving (Show)

data Game = Game {
    world             :: TVar W.World
  , commandsToProcess :: TVar [(Integer, W.Command)]
  , lastTimeAcked     :: TVar (Map.Map Integer TimestampedWorld)
}

newGame :: STM Game
newGame = do
  wd <- newTVar W.newWorld
  cmds <- newTVar []
  lastTimeAcked <- newTVar Map.empty
  return $ Game wd cmds lastTimeAcked


registerUser :: Game -> Text -> STM Integer
registerUser Game{..} name = do
  w <- readTVar world
  let (playerId, newWorld) = W.addCell name w
  writeTVar world newWorld
  return playerId

update :: RandomGen g => Game -> g -> STM W.World
update Game{..} g = do
  w <- readTVar world
  cmds <- readTVar commandsToProcess
  writeTVar commandsToProcess []
  let newWorld = fst $ W.update cmds w g
  writeTVar world newWorld
  lTA <- readTVar lastTimeAcked
  let nLTA = foldr (\(playerId, W.SetTarget timestamp _)
                        -> Map.insert playerId (TimestampedWorld timestamp (W.cullWorld playerId newWorld))) lTA cmds
  writeTVar lastTimeAcked nLTA
  return newWorld

pushCommand :: Game -> Integer -> W.Command -> STM ()
pushCommand Game{..} playerId command = do
  cmds <- readTVar commandsToProcess
  writeTVar commandsToProcess $ (playerId, command):cmds

run :: Game -> IO ThreadId
run game = forkIO . forever $ do
    start <- getTime Monotonic
    stdGen <- getStdGen
    world <- atomically $ update game stdGen
    end <- getTime Monotonic
    let deltaTot = toNanoSecs (diffTimeSpec end start) `div` 1000
    -- putStrLn $ "Sim loop update time:" ++ show deltaTot
    threadDelay (20000 - max 0 (fromIntegral deltaTot))

getAckTime :: Game -> Integer -> STM (Maybe TimestampedWorld)
getAckTime Game{..} playerId = do
  lTA <- readTVar lastTimeAcked
  writeTVar lastTimeAcked $ Map.delete playerId lTA
  let res = if Map.member playerId lTA
            then Just $ lTA Map.! playerId
            else Nothing
  return res

getCulledWorld :: Game -> Integer -> STM W.World
getCulledWorld Game{..} playerId =
    fmap  (W.cullWorld playerId)  (readTVar world)
