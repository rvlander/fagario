{-# LANGUAGE OverloadedStrings #-}

module Protocol (
  Command(..),
  Response(..)
) where

import           Data.Aeson       as A
import           Data.Aeson.Types (typeMismatch)
import           Data.Text        (Text)
import qualified World            as W

type Pseudo = Text;

data Command =
  GameCommand W.Command
  | NewSession Pseudo
  | ResumeSession Integer


instance FromJSON W.Command where
  parseJSON = withObject "bad command" $ \o -> do
    kind <- o .: "type"
    case kind of
      "set_target" -> W.SetTarget <$> o .: "timestamp"  <*> o .: "target"
      _        -> fail ("unknown command type: " ++ kind)


instance FromJSON Command where
  parseJSON = withObject "bad command" $ \o -> do
    kind <- o .: "type"
    case kind of
      "game_command" -> GameCommand <$> o .: "game_command"
      "new_session" -> NewSession <$> o .: "pseudo"
      "resume_session"   -> ResumeSession <$> o .: "playerId"
      _        -> fail ("unknown command type: " ++ kind)



data Response =
  SessionCreatedOK Integer
  | SessionCreatedFailed
  | NewWorldSate Integer W.World

instance ToJSON Response where
  toJSON (SessionCreatedOK playerId) =
    object ["type" .= A.String "session_created_ok", "playerId" .= playerId ]
  toJSON SessionCreatedFailed =
    object ["type" .= A.String "session_created_failed" ]
  toJSON (NewWorldSate timestamp world) =
    object ["type" .= A.String "new_world_state"
      , "timestamp" .= timestamp
      , "world" .= world ]
