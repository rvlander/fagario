{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

import           Config                         (getConfig, port)
import           Control.Concurrent
import           Control.Concurrent.STM         (TVar, atomically, modifyTVar',
                                                 newTVar, readTVar)
import           Control.Exception              (finally)
import           Control.Monad                  (forever)
import           Data.Aeson
import           Data.Aeson.Encode.Pretty
import qualified Data.ByteString.Lazy.Char8     as B
import           Data.Char                      (isPunctuation, isSpace)
import           Data.Monoid                    (mappend)
import           Data.Text                      (Text)
import qualified Data.Text                      as T
import qualified Data.Text.IO                   as T
import           Debug.Trace                    (trace)
import qualified Game                           as G
import           GHC.Generics
import           Network.Wai
import           Network.Wai.Application.Static
import           Network.Wai.Handler.Warp       (run)
import           Network.Wai.Handler.WebSockets
import qualified Network.WebSockets             as WS
import qualified Protocol                       as P
import qualified World                          as W


data Session = Session {
  playerId :: Integer,
  secret   :: Text,
  socket   :: WS.Connection
}
type ServerState = [Session]

newServerState :: ServerState
newServerState = []

addSession :: Session -> ServerState -> ServerState
addSession client clients = client : clients

removeSession :: Session -> ServerState -> ServerState
removeSession client = filter ((/= playerId client) . playerId)

main :: IO ()
main = do
   config <- getConfig "config.json"
   let bindPort = port config
   putStrLn "========================================="
   putStrLn "= ===  === =============================="
   putStrLn "= ==  = = = =============================="
   putStrLn "= =  =     = ==========================="
   putStrLn "========================================="
   putStrLn "\n=== Configuration:"
   B.putStrLn (encodePretty config)
   state <- atomically $ newTVar newServerState
   game <- atomically G.newGame
   G.run game
   putStrLn $ "\n=== Server listening on port" ++ show bindPort ++ "/ ..."
   run bindPort $ app state game

app :: TVar ServerState -> G.Game -> Application
app state game = websocketsOr WS.defaultConnectionOptions wsApp backupApp
   where
     wsApp = application state game
     backupApp = staticApp $ defaultFileServerSettings "./static"

application :: TVar ServerState -> G.Game -> WS.ServerApp
application state game pending = do
   conn <- WS.acceptRequest pending
   WS.forkPingThread conn 30
   msg <- WS.receiveData conn

   case decode msg :: Maybe P.Command of
       Just (P.NewSession pseudo) -> do
              playerId <- atomically $ G.registerUser game pseudo
              session <- createSession playerId conn
              atomically $ modifyTVar' state $ addSession session
              WS.sendTextData conn $ encode $ P.SessionCreatedOK playerId
              flip finally (disconnect session state) $ talk game session
       _ -> return ()

createSession :: Integer -> WS.Connection -> IO Session
createSession playerId conn = do
  -- generate secret
  return $ Session playerId "secret" conn


disconnect :: Session -> TVar ServerState -> IO ()
disconnect session state = atomically $ modifyTVar' state $
    removeSession session

talk :: G.Game -> Session -> IO ()
talk game s@Session{..} =
  let
    sendLoop = forkIO . forever $ sendUpdatedWorld game s >> threadDelay 40000
  in
    sendLoop >> receiveCommands game s

receiveCommands :: G.Game -> Session -> IO()
receiveCommands game Session{..} = forever $ do
  msg <- WS.receiveData socket
  case decode msg :: Maybe P.Command of
    Just (P.GameCommand gameCommand) ->
      atomically $ G.pushCommand game playerId gameCommand
    _ -> T.putStrLn "not a valid game command"

sendUpdatedWorld :: G.Game -> Session -> IO ()
sendUpdatedWorld game Session{..} =
     let
        sendCulledWorld timestamp culledWorld =
          WS.sendTextData socket $ encode $ P.NewWorldSate timestamp culledWorld
     in
      do
        timestamp <- atomically $ G.getAckTime game playerId
        case timestamp of
          Just (G.TimestampedWorld t world) ->
            sendCulledWorld t world
          _ -> return ()
