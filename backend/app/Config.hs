{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Config (
  getConfig,
  port
) where

import           Data.Aeson
import qualified Data.ByteString.Lazy as LB
import           Data.List            (find)
import           Data.Maybe
import           GHC.Generics
import           System.Environment   (getEnvironment)
import           Text.Read

data Config = Config {
  port :: Int
} deriving (Generic, Show)

defaultConfig :: Config
defaultConfig = Config 9160

instance ToJSON Config where
    toEncoding = genericToEncoding defaultOptions

instance FromJSON Config

getConfig :: FilePath -> IO Config
getConfig filepath = do
    contents <- LB.readFile filepath
    defaultConfig <- return $ fromMaybe defaultConfig $ decode contents
    env <- getEnvironment
    return $ buildConfig defaultConfig env



getEnvOrDefault :: [(String, String)] -> (String -> Maybe b) -> b -> String -> b
getEnvOrDefault env conv fallback name =
  fromMaybe fallback $ find (\x -> name == fst x) env >>= conv . snd


buildConfig :: Config -> [(String, String)] -> Config
buildConfig defaultConfig@Config{..} env =
  defaultConfig { port = getEnvOrDefault env readMaybe port "PORT"}
