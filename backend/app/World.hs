{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module World (
    update
  , Command (..)
  , World
  , newWorld
  , addCell
  , Vec2
  , cullWorld
) where

import           Data.Aeson
import           Data.Aeson.Types     (typeMismatch)
import           Data.List            (find, mapAccumR, unfoldr)
import           Data.Monoid
import           System.Random        (RandomGen, randomR)

import           Data.Text            (Text)
import qualified Data.Text            as T
import           Data.Vect.Float.Base (Vec2 (..), norm, zero, (&*), (&+), (&-))
import           Data.Word            (Word8)
import           Debug.Trace
import           GHC.Generics
import           Utilities            (clamp, mapWithMask)

instance FromJSON Vec2 where
    parseJSON (Object v) = Vec2 <$>
                           v .: "x" <*>
                           v .: "y"
    -- A non-Object value is of the wrong type, so fail.
    parseJSON invalid          = typeMismatch "Vec2" invalid

instance ToJSON Vec2 where
    -- this generates a Value
    toJSON (Vec2 x y) =
        object ["x" .= x, "y" .= y]

    -- this encodes directly to a bytestring Builder
    toEncoding (Vec2 x y) =
        pairs ("x" .= x <> "y" .= y)


defaultWorldSize :: Vec2
defaultWorldSize = Vec2 100 100

cellBaseSpeed :: Float
cellBaseSpeed = 0.05

foodElementMass :: Float
foodElementMass = 0.3

maxFoodElements :: Int
maxFoodElements = 1000

data Color = Color{
  r :: !Word8,
  g :: !Word8,
  b :: !Word8
}deriving (Generic, Show)

instance ToJSON Color where
  toEncoding = genericToEncoding defaultOptions

data Cell =  Cell {
    name     :: Text
  , position :: Vec2
  , velocity :: Vec2
  , mass     :: Float
  , radius   :: Float
  , color    :: Color
  , playerId :: Integer
}deriving (Generic, Show)

instance ToJSON Cell where
  toEncoding = genericToEncoding defaultOptions

data FoodElement = FoodElement {
    fposition :: Vec2
--  , fcolor    :: Color
} deriving (Generic, Show)

instance ToJSON FoodElement where
  toEncoding = genericToEncoding defaultOptions

data World = World{
    cells  :: [Cell]
  , food   :: [FoodElement]
  , size   :: Vec2
  , nextId :: Integer
} deriving (Generic, Show)

instance ToJSON World where
  toEncoding = genericToEncoding defaultOptions

data Command =
    SetTarget Integer Vec2;

newWorld :: World
newWorld = World [] [] defaultWorldSize 0

newCell :: Text -> Integer -> Cell
newCell name playerId = Cell {
  name = name,
  position = Vec2 10 10,
  velocity = Vec2 0 0,
  mass = 10,
  radius = 1.784,
  color = Color 150 150 150,
  playerId = playerId
}



cullWorld :: Integer -> World -> World
cullWorld playerId' world@World{food, cells} =
  let
    cell = head $ filter (\Cell{playerId} ->  playerId == playerId') cells
  in
    world {food = cullFood (position cell) food}

cullFood :: Vec2 -> [FoodElement] -> [FoodElement]
cullFood (Vec2 centerX centerY)  =
      filter
        (\(FoodElement (Vec2 cellX cellY)) ->
            let
                boxWidth = 20 -- TODO: parametrize with frontend
                boxHeight = 10
                minX =
                    centerX - boxWidth

                maxX =
                    centerX + boxWidth

                minY =
                    centerY - boxHeight

                maxY =
                    centerY + boxHeight

                radius =
                    foodElementMass
            in
                cellX
                    + radius
                    > minX
                    && cellX
                    - radius
                    < maxX
                    && cellY
                    + radius
                    > minY
                    && cellY
                    - radius
                    < maxY
        )

generateNewFoodElement :: RandomGen g => g -> (FoodElement, g)
generateNewFoodElement g =
    let
      (pos, g') = generateRandomPosition defaultWorldSize g
      (color, g'') = generateRandomColor g'
    in (FoodElement pos, g'') --Removed color. TODO: add it

generateRandomPosition :: RandomGen g => Vec2 -> g -> (Vec2, g)
generateRandomPosition (Vec2 worldWidth worldHeight)  g =
    let
      (x, g') = randomR (0, worldWidth) g
      (y, g'') = randomR (0, worldHeight) g'
    in (Vec2 x y, g'')

generateRandomColor :: RandomGen g => g -> (Color, g)
generateRandomColor g =
  let
    (r, g') = randomR (0, 255) g
    (v, g'') = randomR (0, 255) g'
    (b, g''') = randomR (0, 255) g''
  in (Color r v b, g'')


addCell :: Text -> World -> (Integer, World)
addCell name world@World{cells, nextId} =
  (nextId, world { nextId = nextId + 1, cells = newCell name nextId : cells })


updateVelocity :: Cell -> Vec2 -> Cell
updateVelocity cell@Cell{position} target = let
      delta = target &- position
      nrm = norm delta
    in
      cell { velocity = delta &* (cellBaseSpeed / nrm)}

clampVec2 :: Vec2 -> Vec2 -> Vec2 -> Vec2
clampVec2 (Vec2 ox oy) (Vec2 mx my) (Vec2 x y) = Vec2 (clamp ox mx x) (clamp oy my y)

clampToWorld :: Vec2 -> Vec2
clampToWorld = clampVec2 zero defaultWorldSize

eats :: Cell -> FoodElement -> Bool
c@Cell{..} `eats` FoodElement{fposition} =
  radius > foodElementMass + norm (fposition &- position)

addMass :: Float -> Cell -> Cell
addMass m c@Cell{mass} =
  let
    newMass = m + mass
  in
    c { mass = newMass, radius = sqrt (mass / pi)}

handlePhysics :: World -> World
handlePhysics w@World{..} =
  let moveCell c@Cell{..} =
        c {  position = clampToWorld $ position &+ velocity}
      cellsMeal c = foldr (\foodElement (foodElements, c@Cell{mass})
         -> case c `eats` foodElement of
              True -> (foodElements, addMass 0.3 c)
              _ -> (foodElement:foodElements, c)) ([], c)
      (newFoodElements, newCells) = mapAccumR (flip cellsMeal) food cells
  in
    w { cells = map moveCell newCells, food =  newFoodElements}

handleFoodCreation :: RandomGen g => World -> g -> (World, g)
handleFoodCreation world@World{food} g =
              case length food of
                n | n>= maxFoodElements -> (world, g)
                  | otherwise -> let
                        (foodElement, g') = generateNewFoodElement g
                      in
                        handleFoodCreation (world {food = foodElement:food })  g'


update :: RandomGen g => [(Integer, Command)] -> World -> g -> (World, g)
update commands world =
  let
    handleCommands = foldl handleCommand world
    handleCommand world@World{cells} (playerId', command) = case command of
      SetTarget _ target
        -> world { cells = mapWithMask (\Cell{playerId} ->  playerId == playerId')
                                              (`updateVelocity` target) cells }
  in
    handleFoodCreation (handlePhysics $ handleCommands commands)
